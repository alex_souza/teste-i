package br.com.alex;

import java.io.InputStreamReader;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrincipalTest {
	
	private Principal principal;
	InputStreamReader is;
	String valorTeste = new String();
	
	@Before
	public void before() {		
		this.principal = new Principal();
	}
	
	private char testFirstChar(Stream stream) {
		char c = principal.firstChar(stream);
		return c;
	}
	
	@Test
	public void testFirstChar() {
		try {
			
			char c = testFirstChar(new VerificaVogal("aAbBABac"));

			Assert.assertEquals('b', c);
			
		} catch (Exception e) {
			Assert.fail(e.getLocalizedMessage());
		}
	}
	

}
