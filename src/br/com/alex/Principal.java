package br.com.alex;

import java.util.LinkedHashMap;

import br.com.alex.Stream;
import br.com.alex.VerificaVogal;

public class Principal {

	public static char firstChar(Stream input) {
		int verificaPrimeiraConsoante = 0;
		int confirmaPrimeiraVogal = 0;
		int index = 0;
		char vogalNaoRepete = 0;
		
		LinkedHashMap<Character, Integer> texto = new LinkedHashMap<Character, Integer>();

		while (input.hasNext()) {
			Character letra = input.getNext();

			if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u' ||
					letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U') {
				if (!texto.containsValue(letra)) {
					if (verificaPrimeiraConsoante > 0 && confirmaPrimeiraVogal == 0) {
						vogalNaoRepete = letra;
						confirmaPrimeiraVogal ++;
					}
				}
				texto.put(letra, index++);
			} else {
				verificaPrimeiraConsoante ++;
			}
		}
		return vogalNaoRepete;
	}
	
	public static void main(String[] args) {
		VerificaVogal verifica = new VerificaVogal("aAbBABacfe");
		char resposta = firstChar(verifica);
		System.out.println("Saída: " + resposta);
	}
}