# README #

Dada uma stream, encontre o primeiro vogal que não se repite e vem logo após a primeira consoante. O termino da leitura da stream deve ser garantido atraves do metodo hasNext(), ou seja, retorna falso para o termino da leitura da stream. Voce tera acesso a leitura da stream atraves dos metodos de interface fornecidos ao termino do enunciado.

Premissas: Uma chamada para hasNext() ir retornar se a stream ainda contem caracteres para processar. Uma chamada para getNext() ir retornar o proximo caracter a ser processado na stream. Nao sera possivel reiniciar o fluxo da leitura da stream. Exemplo:

Input: aAbBABacfe

Output: e