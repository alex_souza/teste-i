package br.com.alex;

public class VerificaVogal implements Stream {
		private char[] arrayTexto;
		private int tamanho;
		private int indice;
		
		VerificaVogal(String texto) {
			if (texto == null || texto.equalsIgnoreCase("")) {
				System.out.println("Informe o conjunto de caracteres.");
			}
			this.arrayTexto = texto.toCharArray();
			tamanho = texto.length();
			indice = 0;
		}

		@Override
		public char getNext() {
			return arrayTexto[indice];
		}

		@Override
		public boolean hasNext() {
			return indice < tamanho;
		}
}
