package br.com.alex;

public interface Stream {
	
	char getNext();
	
	boolean hasNext();
}
